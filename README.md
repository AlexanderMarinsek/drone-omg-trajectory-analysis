## OMG-tools trajectory analysis
**Written in Python.**

Analysis of flight data obtained while guiding a multirotor using OMG-tools.
Comparison of measured position and speed data with OMG-tools' estimate,
represented in the form of 2D and 3D plots.

---

https://bitbucket.org/AlexanderMarinsek/drone-omg-trajectory-analysis/
git clone git@bitbucket.org:AlexanderMarinsek/drone-omg-trajectory-analysis.git
