import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from matplotlib.colors import ListedColormap, BoundaryNorm
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Line3DCollection
import csv
from math import sqrt



# Calculate the average absolute error in between two datasets
def calc_avg_abs_error(data1, data2):
    e = 0.
    # Accumulate absolute error
    for i in range (0, len(data1)):
        e += abs(data1[i] - data2[i])
    # Average
    return e / i



def analyze(input_file, vel_const, output_name, showfig=True):

    # Generate position arrays with dummy initial value
    position_drone = np.c_[[0., 0., 0.]]
    position_omg = np.c_[[0., 0., 0.]]
    # Generate velocity arrays with dummy initial value
    velocity_drone = np.c_[[0., 0., 0.]]
    velocity_omg = np.c_[[0., 0., 0.]]
    # Absolute velocity arrays
    abs_v_d = np.array([0.])
    abs_v_o = np.array([0.])

    # Read data file and transfer readings to working memory
    with open(input_file, 'r') as csvfile:
        # View file as CSV file
        data_reader = csv.reader(csvfile, delimiter=',')
        # Skip first line
        next(data_reader)

        for row in data_reader:

            # Drone's actual position
            position_drone_tmp = \
                np.c_[[float(row[6]), float(row[7]), float(row[8])]]
            position_drone = np.c_[position_drone, position_drone_tmp]

            # OMG's position estimate
            position_omg_tmp = \
                np.c_[[float(row[12]), float(row[13]), float(row[14])]]
            position_omg = np.c_[position_omg, position_omg_tmp]

            # Drone's actual velocity
            velocity_drone_tmp = \
                np.c_[[float(row[9]), float(row[10]), float(row[11])]]
            velocity_drone = np.c_[velocity_drone, velocity_drone_tmp]

            # OMG's velocity estimate
            velocity_omg_tmp = \
                np.c_[[float(row[15]), float(row[16]), float(row[17])]]
            velocity_omg = np.c_[velocity_omg, velocity_omg_tmp]

            # Drones's absolute velocity
            abs_v_d_tmp = \
                sqrt(float(row[9])**2 + float(row[10])**2 + float(row[11])**2)
            abs_v_d = np.append(abs_v_d, abs_v_d_tmp)

            # OMG's absolute velocity estimate
            abs_v_o_tmp = \
                sqrt(float(row[15])**2 + float(row[16])**2 + float(row[17])**2)
            abs_v_o = np.append(abs_v_o, abs_v_o_tmp)

    # Throw away initial dummy values (and mot. planning sript's initial guess)
    position_drone = position_drone[:,2:]
    position_omg = position_omg[:,2:]
    velocity_drone = velocity_drone[:,2:]
    velocity_omg = velocity_omg[:,2:]
    abs_v_d = abs_v_d[2:]
    abs_v_o = abs_v_o[2:]

    # Break out multidimensional arrays into coordinates (NED -> XYZ)
    x_p_d = position_drone[1, :]
    y_p_d = position_drone[0, :]
    z_p_d = -position_drone[2, :]

    x_v_d = velocity_drone[1, :]
    y_v_d = velocity_drone[0, :]
    z_v_d = -velocity_drone[2, :]

    x_p_o = position_omg[1, :]
    y_p_o = position_omg[0, :]
    z_p_o = -position_omg[2, :]

    x_v_o = velocity_omg[1, :]
    y_v_o = velocity_omg[0, :]
    z_v_o = -velocity_omg[2, :]

    """
    print "%s, %s, %s, %s, %s" % \
        (str(np.shape(position_drone)),
        str(np.shape(velocity_drone)),
        str(np.shape(position_omg)),
        str(np.shape(abs_v_d)),
        str(np.shape(abs_v_o)))

    print "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s" % \
        (str(np.shape(x_p_d)),
        str(np.shape(x_p_o)),
        str(np.shape(y_p_d)),
        str(np.shape(y_p_o)),
        str(np.shape(z_p_d)),
        str(np.shape(z_p_o)),
        str(np.shape(x_v_d)),
        str(np.shape(x_v_o)),
        str(np.shape(y_v_d)),
        str(np.shape(y_v_o)),
        str(np.shape(z_v_d)),
        str(np.shape(z_v_o)))
    """

    # Get axis limits for drawing drone and Omg position on same plot (+10%)
    max_x = np.array([x_p_d, x_p_o]).max() * 1.10
    min_x = np.array([x_p_d, x_p_o]).min() * 1.10
    max_y = np.array([y_p_d, y_p_o]).max() * 1.10
    min_y = np.array([y_p_d, y_p_o]).min() * 1.10
    max_z = np.array([z_p_d, z_p_o]).max() * 1.10
    min_z = np.array([z_p_d, z_p_o]).min() * 0.90


    # Set global plot parameters
    mpl.rcParams['image.cmap'] = 'viridis'
    # Set global plot syle
    plt.style.use('default')
    #plt.style.use('seaborn')

    # 3D figure ****************************************************************
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # Set axis limits
    ax.set_xlim(min_x, max_x)
    ax.set_ylim(min_y, max_y)
    ax.set_zlim(min_z, max_z)

    # Obtain 3D segments for curve drawing
    points1 = np.array([x_p_d, y_p_d, z_p_d]).T.reshape(-1, 1, 3)
    segments1 = np.concatenate([points1[:-1], points1[1:]], axis=1)

    points2 = np.array([x_p_o, y_p_o, z_p_o]).T.reshape(-1, 1, 3)
    segments2 = np.concatenate([points2[:-1], points2[1:]], axis=1)

    # Get minimum and maximum value for plot's color scale
    c_min = np.array([abs_v_d.min(), abs_v_o.min()]).min()
    c_max = np.array([abs_v_d.max(), abs_v_o.max()]).max()

    # Normalize (map) color scale to data set
    norm = plt.Normalize(c_min, c_max)

    # Apply color scale to line segments and get line collection for plotting
    lc1 = Line3DCollection(segments1, cmap='viridis', norm=norm)
    lc2 = Line3DCollection(segments2, cmap='magma', norm=norm)

    # Dataset 1:
    # Apply data to color scale
    lc1.set_array(abs_v_d)
    # Set line width
    lc1.set_linewidth(2)
    # Obtain color scale handler (would add line collection to plot 2D)
    line1 = ax.add_collection(lc1)
    # Add line collection to 3D plot
    ax.add_collection3d(lc1, zs=z_p_d, zdir='z')
    # Add color scale legend to figure
    clb1 = fig.colorbar(line1, ax=ax, panchor=(0.3, 0.3))
    # Add legend (title of color scale)
    clb1.ax.set_title('Drone [m/s]', fontsize=10)

    # Dataset 2:
    lc2.set_array(abs_v_o)
    lc2.set_linewidth(2)
    line2 = ax.add_collection(lc2)
    ax.add_collection3d(lc2, zs=z_p_o, zdir='z')
    clb2 = fig.colorbar(line2, ax=ax)
    clb2.ax.set_title('OMG [m/s]', fontsize=10)

    fig.suptitle('Trajectory with abs. velocity', fontsize=18, x=0.32)

    # Delete figure padding
    plt.tight_layout()

    figname = 'figures/%s_plot_3d.png' % output_name

    plt.savefig(figname, dpi=600, facecolor='w', edgecolor='w',
                orientation='landscape')
    if showfig:
      plt.show()


    # **************************************************************************


    # Calculate average absolute errors
    aa_error = [calc_avg_abs_error(x_p_d, x_p_o),
                calc_avg_abs_error(y_p_d, y_p_o),
                calc_avg_abs_error(z_p_d, z_p_o),
                calc_avg_abs_error(x_v_d, x_v_o),
                calc_avg_abs_error(y_v_d, y_v_o),
                calc_avg_abs_error(z_v_d, z_v_o)]

    # X - axis values
    t = np.linspace(0, len(x_p_d)-1, num=len(x_p_d))

    # Set global plot syle
    plt.style.use('seaborn')

    # 2D figure ****************************************************************

    # Define new figure with 6 subplots (3D positioning, 3D velocity)
    fig, ax = plt.subplots(nrows=3, ncols=2, sharex=True, sharey=False)

    # Define array containing multiple sub-arrays:
    #  subplot handler, subpl-, x- and y- names, abs. constraint, x, y1 and y2
    subplots = [
        [ax[0, 0], 'Position', 'N', 'x (N) [m]', 0, t, x_p_d, x_p_o],
        [ax[1, 0], '', 'N', 'y (E) [m]', 0, t, y_p_d, y_p_o],
        [ax[2, 0], '', 'N', 'z (-D) [m]', 0, t, z_p_d, z_p_o],
        [ax[0, 1], 'Speed', 'N', 'x (N) [m/s]', vel_const, t, x_v_d, x_v_o],
        [ax[1, 1], '', 'N', 'y (E) [m/s]', vel_const, t, y_v_d, y_v_o],
        [ax[2, 1], '', 'N', 'z (-D) [m/s]', vel_const, t, z_v_d, z_v_o]]


    for k, su in enumerate(subplots):

        # Break up input array into datasets
        ax = su[0]          # Handler
        s_title = su[1]     # Subplot title (used as column title)
        x_l = su[2]         # x-ax title
        y_l = su[3]         # y-ax title
        abs_const = su[4]   # Absolute dataset constraint
        x = su[-3]          # X-ax dataset
        y1 = su[-2]         # Y-ax dataset1
        y2 = su[-1]         # Y-ax dataset2

        # Start with zero constraint violations, regardless of dataset
        viol_counter_y1 = viol_counter_y2 = 0

        # Determine limits for both axes
        min_x = x.min()
        max_x = x.max()
        min_y = np.array([y1, y2]).min()
        max_y = np.array([y1, y2]).max()

        # Find maximal mismatch
        delta_max = 0.
        delta_i = 0

        # Iterate input data to find mismatch
        for i in range(0, len(y1)):
            diff_abs = abs(y1[i]-y2[i])
            if diff_abs > delta_max:
                delta_max = diff_abs
                delta_i = i

        # Plot both datasets
        if abs_const:
            # Calculate segments for 1st dataset
            points = np.array([x, y1]).T.reshape(-1, 1, 2)
            segments = np.concatenate([points[:-1], points[1:]], axis=1)

            cmap = ListedColormap(['#cf000f', '#26a65b', '#cf000f'])
            norm = BoundaryNorm([-1.e+10, -abs_const, abs_const, 1.e+10], cmap.N)
            lc = LineCollection(segments, cmap=cmap, norm=norm)
            lc.set_array(y1)
            ax.add_collection(lc)

            # Calculate segments for 2nd dataset
            points = np.array([x, y2]).T.reshape(-1, 1, 2)
            segments = np.concatenate([points[:-1], points[1:]], axis=1)

            cmap = ListedColormap(['#f7ca18', '#4b77be', '#f7ca18'])
            norm = BoundaryNorm([-1.e+10, -abs_const, abs_const, 1.e+10], cmap.N)
            lc = LineCollection(segments, cmap=cmap, norm=norm)
            lc.set_array(y2)
            ax.add_collection(lc)

            # Iterate both Y-ax datasets and count constraint violations
            for i in range (0, len(y1)):
                if abs(y1[i]) > abs_const:
                    viol_counter_y1 += 1
                if abs(y2[i]) > abs_const:
                    viol_counter_y2 += 1

        else:
            ax.plot(x, y1, color='#26a65b')
            ax.plot(x, y2, color='#4b77be')

        # Compose maximal error box text
        str_me = '%.2f' % round(delta_max,2)

        # Compose average error box and constraint violation text
        str_ae =    'aae=%.2f\n' \
                    'dNe=%d\n'  \
                    'oNe=%d' \
                    % (round(aa_error[k], 2), viol_counter_y1, viol_counter_y2)

        # Plot maximal difference line
        ax.plot([delta_i, delta_i], [y1[delta_i], y2[delta_i]], 'k-',
            linewidth=2)
        # Plot max. difference amount
        ax.text(delta_i, y1[delta_i], str_me,
            bbox={'boxstyle':'round4,pad=0.3', 'facecolor':'red',
                'edgecolor':'black', 'alpha':0.5}, color='w')
        ax.text(min_x, min_y, str_ae,
            bbox={'boxstyle':'round4,pad=0.3', 'facecolor':'blue',
                'edgecolor':'black', 'alpha':0.5}, color='w')

        # Set x and y axis limits
        ax.set_xlim(min_x - 1, max_x + 1)
        ax.set_ylim(min_y, max_y)

        # Set (column) name and axis titles
        if s_title:
            ax.set_title(s_title)
        ax.set_xlabel(x_l);
        ax.set_ylabel(y_l);

    # Set general figure legend
    fig.legend(['Drone', 'OMG', 'error'], frameon=True, loc='lower center',
        ncol=3, edgecolor=(0.2, 0.2, 0.2), facecolor=(0.95, 0.95, 0.95))

    # Add spacing to subplots
    plt.subplots_adjust(wspace=0.35, hspace=0.3)
    # Delete figure padding
    plt.tight_layout()

    # Save figure
    figname = 'figures/%s_coordinates_subplot.png' % output_name
    plt.savefig(figname, dpi=800, edgecolor='w', orientation='landscape')
    if showfig:
      plt.show()


# ******************************************************************************

vel_const = 0.5;
input_file = 'measurements/2019-01-17/1547725361.csv'
output_name = 'vmax_0.5_amax_1.0_v1'
analyze(input_file, vel_const, output_name, showfig=False)

vel_const = 0.5;
input_file = 'measurements/2019-01-17/1547725802.csv'
output_name = 'vmax_0.5_amax_3.0_v1'
analyze(input_file, vel_const, output_name, showfig=False)

vel_const = 0.5;
input_file = 'measurements/2019-01-17/1547725964.csv'
output_name = 'vmax_0.5_amax_3.0_v2'
analyze(input_file, vel_const, output_name, showfig=False)

vel_const = 0.5;
input_file = 'measurements/2019-01-17/1547726244.csv'
output_name = 'vmax_0.5_amax_5.0_v1'
analyze(input_file, vel_const, output_name, showfig=False)

vel_const = 2.5;
input_file = 'measurements/2019-01-17/1547726351.csv'
output_name = 'vmax_2.5_amax_1.0_v1'
analyze(input_file, vel_const, output_name, showfig=False)

vel_const = 2.5;
input_file = 'measurements/2019-01-17/1547725499.csv'
output_name = 'vmax_2.5_amax_2.0_v1'
analyze(input_file, vel_const, output_name, showfig=False)

vel_const = 5.0;
input_file = 'measurements/2019-01-17/1547725617.csv'
output_name = 'vmax_5.0_amax_3.0_v1'
analyze(input_file, vel_const, output_name, showfig=False)

vel_const = 5.0;
input_file = 'measurements/2019-01-17/1547726590.csv'
output_name = 'vmax_5.0_amax_3.0_v2'
analyze(input_file, vel_const, output_name, showfig=False)

print "* Calculation finished"
